const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({

	firstName:{
		type: String,
		required: [true, "First name is required"]
	},
	lastName:{
		type: String,
		required: [true, "First name is required"]
	},
	email:{
		type: String,
		required: [true, "First name is required"]
	},
	password:{
		type: String,
		required: [true, "First name is required"]
	},
	mobileNo:{
		type: String,
		required: [true, "First name is required"]
	},
	isAdmin:{
		type: Boolean,
		default: false
	},
	order:[
	{	
		totalAmount:{
			type: Number,
			required: [true, "Total amount is required"]
		},
		purchasedOn:{
			type: Date,
			default: new Date()
		},
		
		
			prodId:{
			type: String,
			required: [true, "Product ID is required"]
		},
			quantity: {
				type: Number,
				required: [true, "Quantity is required"]
			}
		
		
	}]

})

module.exports = mongoose.model("User", userSchema)