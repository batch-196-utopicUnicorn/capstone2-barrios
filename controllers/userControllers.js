const User = require("../models/User");
const Product = require('../models/Product')
const bcrypt = require('bcrypt');
const auth = require('../authentication')


//console.log("Connected to userControllers")
module.exports.registerUser = (req, res) => {

	const hashedPassword = bcrypt.hashSync(req.body.password, 10);
	//console.log(hashedPassword);

	let newUser = new User({

		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		password: hashedPassword,
		mobileNo: req.body.mobileNo
	});

	newUser.save().then(result => res.send(result)).catch(error => res.send(error))
};

module.exports.getAllUsers = (req, res) => {

	User.find({})
	.then(result => res.send(result))
	.catch(err => res.send(err));
};

module.exports.loginUser = (req, res) => {
	//console.log(req.body);

	User.findOne({email:req.body.email}).then(result => {
		if(result === null){
			res.send({message:"User not found."})
		}else{
			//console.log('first else')
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);
			if (isPasswordCorrect) {
				//console.log({accessToken: auth.createAccessToken(result)})
				res.send({accessToken: auth.createAccessToken(result)})
			}else{
				//console.log('second else')
				res.send({message: "Incorrect password"})
			}
		}
	})
}

module.exports.getUserDetails = (req, res) => {
	//console.log("Checking")
	//console.log(req.user)
	User.findById(req.user.id)
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

module.exports.toAdmin = (req, res) => {

	User.findByIdAndUpdate(req.params.userId, {isAdmin: true}, {new:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

module.exports.createOrder = async (req, res) => {
	if(req.user.isAdmin){
		console.log({message: "Action Forbidden"})
		return res.send(false)
	}else{
		let isUserUpdated = await User.findById(req.user.id).then(user =>{
			/*console.log(user)*/
			console.log(req.body.prodId)
			let newOrder = {
				prodId: req.body.prodId,
				quantity: 1,
				totalAmount: req.body.totalAmount

				/*,
				userId: req.user.id,
				
				totalAmount: req.body.totalAmount,
				products: [{
					prodId: req.body.prodId,
					quantity: 1
				}*/
				
			}
			

			user.order.push(newOrder)
			console.log(newOrder)
			return user.save()
			//.then(user => res.send(user))
			.then(user => true)
			.catch(err => err.message)
		})

		if(isUserUpdated !== true){
			return res.send({message: 'User is not updated'})
		}else{
			//console.log("goods")
			let isProductUpdated = await Product.findById(req.body.prodId).then(product => {

				let customer = {
					userId: req.user.id,
					quantity: 1
				}
				
				product.order.push(customer)
				//console.log(product)
				
				return product.save().then(product => true).catch(err => err.message)
			})

			if(isUserUpdated !== true){
				return res.send({message: isProductUpdated})
			}
			
		if(isUserUpdated && isProductUpdated){
			return res.send({message: "Order has been created"})
		}
		}
	}
}

module.exports.getUserOrders = (req, res) => {
	if(req.user.isAdmin){

		return res.send({message: "Action Forbidden"})

	}else{
	
		User.findById(req.user.id)
		.then(result => res.send(result.order))
}
}

module.exports.allUsersOrders = (req, res) =>{

	// User.find({})
	// .then(result => {
	// 	/*result.forEach(res => {
	// 		for(let key in res){
	// 			console.log(result)
	// 		}
	// 	})*/
	// 	result.forEach((results) =>{
	// 		if(results.order !== []){
	// 			.then(resh => res.send(resh))
	// 		}
			
	// 	})
	// })
	// .catch(error => res.send(error))
	//User.find({isAdmin:false},{order:1})
	//.then(result => res.send(result))

	User.find({})
     .then(result=>{
        let allUserOrders = result.map(function(field){

           if(field.isAdmin === true){
            return
           } else if(field.order.length===0){
            return
           } else {
            return field.order
           }

        })
        let filteredOrders = allUserOrders.filter(display=>display!= null)
        res.send(filteredOrders)
     }).catch(error=>res.send(error))


}

module.exports.checkUserEmail = (req, res) => {
	User.findOne({email:req.body.email})
	.then(result => {

		//findOne will result null if no match is found
		//send false if email does not exist
		//send true if email exist
		if(result === null){
			return res.send(false)
		}else {
			return res.send(true)
		}
	})

	.catch(error => res.send(error))
}
