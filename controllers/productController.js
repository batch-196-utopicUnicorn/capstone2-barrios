const Product = require('../models/Product');
const User = require('../models/User')


module.exports.addProduct = (req, res) => {

	let newProd = new Product({

		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	})
	console.log(newProd)
	newProd.save()
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

module.exports.getAllProduct= (req,res) => {

	Product.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error));

}

module.exports.getAllActiveProduct = (req, res) => {

	Product.find({isActive: true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

module.exports.singleProduct = (req, res) => {
	//console.log(req.params.prodId)

	Product.findById(req.params.prodId)
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

module.exports.updateProduct = (req, res) => {

	let update = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	}

	Product.findByIdAndUpdate(req.params.prodId, update, {new:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

module.exports.archive = (req, res) => {

	Product.findByIdAndUpdate(req.params.prodId, {isActive: false}, {new: true})
	//.then(result => console.log(result))
	.then(result => res.send(result))
	.catch(error => res.send(error))

}

module.exports.unArchive = (req, res) => {

	Product.findByIdAndUpdate(req.params.prodId, {isActive: true}, {new: true})
	//.then(result => console.log(result))
	.then(result => res.send(result))
	.catch(error => res.send(error))

}