const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const app = express()
const PORT = process.env.PORT || 4000;



mongoose.connect('mongodb+srv://admin:admin123@cluster0.brekg.mongodb.net/WaterSportApi?retryWrites=true&w=majority',
{
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;

db.on('error', console.error.bind(console, "MongoDB connection Error"));
db.once("open", () => console.log("Connected to MongoDB"));

app.use(express.json());
app.use(cors());

const userRoutes = require('./routes/userRoute');
const productRoutes = require('./routes/productRoute')
//const orderRoutes = require('./routes/orderRoutes')

app.use('/users', userRoutes);
app.use('/products', productRoutes)
//app.use('/orders', orderRoutes)

app.listen(PORT, () => console.log('Server is running at port 4000.'));