const express = require('express');
const router = express.Router();
const productControllers = require('../controllers/productController');
const auth = require('../authentication');
const {verify, verifyAdmin} = auth


router.post('/addProduct',verify, verifyAdmin, productControllers.addProduct);
router.get('/getAllProduct', productControllers.getAllProduct);
router.get('/activeProduct', productControllers.getAllActiveProduct);
router.get('/singleProduct/:prodId', productControllers.singleProduct);
router.put('/updateProduct/:prodId', verify, verifyAdmin, productControllers.updateProduct)
router.delete('/archive/:prodId', productControllers.archive)
router.put('/unArchive/:prodId', productControllers.unArchive)

module.exports = router;

/*verify, verifyAdmin, */