const express = require('express');
const router = express.Router();
const userControllers = require('../controllers/userControllers');
//console.log("Connected to userRoute")
const auth = require('../authentication')
const {verify, verifyAdmin} = auth;



router.post('/', userControllers.registerUser);
router.get('/getAllUsers', verify, verifyAdmin, userControllers.getAllUsers);
router.post('/login', userControllers.loginUser);
router.post('/checkEmail', userControllers.checkUserEmail);
router.get('/details', verify, userControllers.getUserDetails);
router.post('/toAdmin/:userId', verify, verifyAdmin, userControllers.toAdmin);
router.post('/createOrder', verify, userControllers.createOrder);
router.get('/getUserOrders', verify, userControllers.getUserOrders);
router.get('/allUsersOrders', verify, verifyAdmin, userControllers.allUsersOrders)

module.exports = router;