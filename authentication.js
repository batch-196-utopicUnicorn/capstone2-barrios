const jwt = require('jsonwebtoken')

const secret = "12345"

//console.log(secret)

module.exports.createAccessToken = (userDetails) => {
	//console.log(userDetails)
	const data = {
		id: userDetails.id,
		email: userDetails.email,
		isAdmin: userDetails.isAdmin
	}
	//console.log(data);
	return jwt.sign(data, secret, {})
}

module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization
	
	if(typeof token === "undefined"){
		return res.send({authorization: "Failed. No token received."})
	}else{
		token = token.slice(7);

		jwt.verify(token, secret, function (error, decodedToken){

			if(error){
				return res.send({
					authorization: "Failed",
					message: error.message
				})
			}else{
				req.user = decodedToken;
				//console.log(token)
				next()
			}
		})
	}
}

module.exports.verifyAdmin = (req, res, next) => {
	//console.log(req.user)

	if(req.user.isAdmin){
		next()
	}else{
		return res.send({
			authorization: "Failed. Not an Admin",
			message: "Action Forbidden"
		})
	}
}